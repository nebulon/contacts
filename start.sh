#!/bin/bash

set -eu

export NODE_ENV=production

if [[ ! -f /app/data/.credentials.json ]]; then
    echo -e "{}" > /app/data/.credentials.json
fi

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start the server"
exec /usr/local/bin/gosu cloudron:cloudron node /app/code/server.js /app/data
