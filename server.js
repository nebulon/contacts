#!/usr/bin/env node

'use strict';

var express = require('express'),
    fs = require('fs'),
    path = require('path'),
    addressbooks = require('./backend/addressbooks.js');

const DATA_DIR = path.resolve(process.argv[2] || __dirname);
const CREDENTIALS_FILE = path.join(DATA_DIR, '.credentials.json');

console.log('Using data dir:', DATA_DIR);

var user = null;
try {
    user = JSON.parse(fs.readFileSync(CREDENTIALS_FILE, 'utf8'));
} catch (e) {
    console.error('Add .credentials.json file');
    process.exit(1);
}

function getAddressbooks(req, res) {
    console.log('getAddressbooks');

    addressbooks.getAll(user, function (error, result) {
        if (error) {
            console.error('Failed to get addressbooks', error);
            return res.status(500).send('failed to get addressbooks');
        }

        res.send({ addressbooks: result });
    });
}

function refreshAddressbooks(req, res) {
    console.log('refreshAddressbooks');

    addressbooks.refresh(user, function (error, result) {
        if (error) {
            console.error('Failed to refresh addressbooks', error);
            return res.status(500).send('failed to refresh addressbooks');
        }

        res.send({ addressbooks: result });
    });
}

var app = express();
app.use('/api/healthcheck', function (req, res) { res.status(200).send(); });
app.get('/api/addressbooks', getAddressbooks);
app.put('/api/addressbooks', refreshAddressbooks);

// these are node_module deps for the frontend
app.use('/3rdparty/axios', express.static(path.join(__dirname, 'node_modules/axios/dist')));
app.use('/3rdparty/vue', express.static(path.join(__dirname, 'node_modules/vue/dist')));
app.use('/3rdparty/primevue', express.static(path.join(__dirname, 'node_modules/primevue')));
app.use('/3rdparty/primeicons', express.static(path.join(__dirname, 'node_modules/primeicons')));
app.use('/3rdparty/primeflex', express.static(path.join(__dirname, 'node_modules/primeflex')));
app.use('/3rdparty/moment', express.static(path.join(__dirname, 'node_modules/moment/min')));

app.use('/', express.static(path.join(__dirname, 'frontend')));

var server = app.listen(3000, function () {
    var host = server.address().address;
    var port = server.address().port;

    addressbooks.initCache(DATA_DIR);

    console.log(`Listening on http://${host}:${port}`);
});
