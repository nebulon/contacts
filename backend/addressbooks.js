'use strict';

exports = module.exports = {
    initCache,
    getAll,
    refresh
};

var assert = require('assert'),
    dav = require('dav'),
    fs = require('fs'),
    md5 = require('md5'),
    path = require('path'),
    vCard = require('vcf');

var cacheFilePath = '';
var cache = {};

function initCache(dataDir) {
    cacheFilePath = path.join(dataDir, '.cache');

    try {
        cache = JSON.parse(fs.readFileSync(cacheFilePath, 'utf8'));
    } catch (e) {
        console.error('Unable to load cache file. Start fresh.', e);
    }
}

function getAll(user, callback) {
    assert.strictEqual(typeof user, 'object');
    assert.strictEqual(typeof callback, 'function');

    if (cache[user] && cache[user].addressbooks) return callback(null, cache[user].addressbooks);

    refresh(user, callback);
}

function refresh(user, callback) {
    assert.strictEqual(typeof user, 'object');
    assert.strictEqual(typeof callback, 'function');

    var xhr = new dav.transport.Basic(new dav.Credentials({
        username: user.username,
        password: user.password
    }));

    dav.createAccount({ server: user.server, xhr: xhr, accountType: 'carddav', loadObjects: true }).then(function (account) {
        const addressbooks = account.addressBooks.map(function (addressbook) {
            console.log('Found address book name ' + addressbook.displayName);

            function get(card, id) {
                var fields = card.get(id);

                // pick first one for now
                if (Array.isArray(fields)) fields = fields[0];

                if (!fields) return '';
                return fields.valueOf();
            }

            return {
                displayName: addressbook.displayName,
                url: addressbook.url,
                contacts: addressbook.objects.map(function (contact) {
                    var card = new vCard().parse(contact.addressData);

                    var email = get(card, 'email');
                    var photo = get(card, 'photo');

                    var address = null;
                    var addressData = get(card, 'adr');
                    if (addressData) {
                        let parts = addressData.split(';');
                        address = {
                            poBox: parts[0],
                            apartment: parts[1],
                            street: parts[2],
                            city: parts[3],
                            region: parts[4],
                            zip: parts[5],
                            country: parts[6],
                        };
                    }


                    return {
                        url: contact.url,
                        displayName: get(card, 'fn'),
                        email: get(card, 'email'),
                        phone: get(card, 'tel'),
                        mobile: get(card, 'mobile'),
                        birthday: get(card, 'bday'),
                        address: address,
                        avatar: photo ? `data:image/png;base64,${photo}` : (email ? ('https://www.gravatar.com/avatar/' + md5(email.trim())) + '?d=mp' : 'https://www.gravatar.com/avatar/00000000000000000000000000000000?d=mp')
                    };
                })
            };
        });

        if (!cache[user]) cache[user] = {};

        cache[user].addressbooks = addressbooks;

        try {
            fs.writeFileSync(cacheFilePath, JSON.stringify(cache), 'utf8');
        } catch (e) {
            console.error('Unable to write cache file.', e);
        }

        callback(null, addressbooks);
    });
}
