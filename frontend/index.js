'use strict';

/* global Vue */
/* global axios */
/* global moment */
/* global button */
/* global toolbar */
/* global inputtext */

Vue.createApp({
    data() {
        return {
            addressbooksLoading: true,
            addressbooks: [],
            activeAddressbook: {},
            activeContact: {},
            contactsQuickSearch: '',
            editContactMode: false,
            editContactData: {}
        }
    },
    components: {
        'p-button': button,
        'p-toolbar': toolbar,
        'p-inputtext': inputtext
    },
    methods: {
        prettyDate(date) {
            return moment(date).format('MMMM Do YYYY');
        },
        prettyAddress(address) {
            if (!address) return '';

            var html = '';
            if (address.poBox) html += address.poBox + '<br/>';
            if (address.apartment) html += address.apartment + '<br/>';
            if (address.street) html += address.street + '<br/>';
            if (address.city && !address.zip) html += address.city + '<br/>';
            if (!address.city && address.zip) html += address.zip + '<br/>';
            if (address.city && address.zip) html += address.city + ' ' + address.zip + '<br/>';
            if (address.region) html += address.region + ' ' + (address.country || '') + '<br/>';

            return html;
        },
        refreshAddressbooks() {
            var that = this;

            that.addressbooksLoading = true;
            that.activeContact = {};

            axios.put('/api/addressbooks').then(function (res) {
                that.addressbooks = res.data.addressbooks;
                that.activeAddressbook = that.addressbooks[0];

                that.addressbooksLoading = false;
            });
        },
        contactsFilter(contacts) {
            var that = this;

            if (!contacts) return [];

            return contacts.filter(function (contact) {
                if (!that.contactsQuickSearch) return true;

                return (contact.displayName.trim().toLowerCase().indexOf(that.contactsQuickSearch.trim().toLowerCase()) !== -1);
            }).sort(function (a, b) {
                var nameA = a.displayName.toUpperCase(); // ignore upper and lowercase
                var nameB = b.displayName.toUpperCase(); // ignore upper and lowercase

                if (nameA < nameB) return -1;
                if (nameA > nameB) return 1;
                return 0;
            });
        },
        startContactEdit() {
            this.editContactData = JSON.parse(JSON.stringify(this.activeContact));
            this.editContactMode = true;
        },
        saveContactEdit() {
            this.editContactData = {};
            this.editContactMode = false;
        },
        cancelContactEdit() {
            this.editContactData = {};
            this.editContactMode = false;
        }
    },
    mounted() {
        var that = this;

        that.addressbooksLoading = true;

        axios.get('/api/addressbooks').then(function (res) {
            that.addressbooks = res.data.addressbooks;
            that.activeAddressbook = that.addressbooks.find(function (addressbook) { return !!addressbook.contacts.length; }) || {};

            that.addressbooksLoading = false;
        });
    }
}).mount('#app');
